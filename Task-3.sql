CREATE TABLE Doctor (
    doctor_id INT PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE Secretary (
    secretary_id INT PRIMARY KEY,
    name VARCHAR(255)
);


CREATE TABLE Prescription (
    prescription_id VARCHAR(255) PRIMARY KEY,
    drug VARCHAR(255),
    dosage VARCHAR(255),
    prescription_date DATE,
    doctor_id INTEGER,
    secretary_id INT,
    FOREIGN KEY (doctor_id)
        REFERENCES Doctor (doctor_id),
    FOREIGN KEY (secretary_id)
        REFERENCES Secretary (secretary_id)
);
CREATE TABLE Patient (
    patient_id VARCHAR(255) PRIMARY KEY,
    name VARCHAR(255),
    dob DATE,
    address TEXT,
    secretary_id INT,
    FOREIGN KEY (prescription_id)
        REFERENCES Prescription (prescription_id)
);