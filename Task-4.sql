CREATE TABLE Secretary (
    secretary_id INT PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE Doctor (
    doctor_id INT PRIMARY KEY,
    name VARCHAR(255)
    secretary_id INT,
     
);

CREATE TABLE Patient (
    patient_id VARCHAR(255) PRIMARY KEY,
    name VARCHAR(255),
    dob DATE,
    address TEXT,
    doctor_id INT,
    FOREIGN KEY (doctor_id)
        REFERENCES Doctor (doctor_id)
);


CREATE TABLE Prescription (
    prescription_id VARCHAR(255) PRIMARY KEY,
    drug VARCHAR(255),
    dosage VARCHAR(255),
    prescription_date DATE,
    patient_id INT,
    FOREIGN KEY (patient_id)
        REFERENCES Patient (patient_id)
);







