CREATE TABLE Book (
    isbn INT PRIMARY KEY,
    title VARCHAR(255),
    author VARCHAR(255),
    publisher VARCHAR(255),
    num_copies INT
);


CREATE TABLE Branch (
    branch VARCHAR(255) PRIMARY KEY,
    Branch_Addr TEXT,
    isbn INTEGER,
    FOREIGN KEY (isbn)
        REFERENCES Book (isbn)
);
