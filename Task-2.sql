CREATE TABLE Manager (
    manager_id INT PRIMARY KEY,
    name VARCHAR(255),
    location VARCHAR(255)
);

CREATE TABLE Staff (
    staff_id INT PRIMARY KEY,
    name VARCHAR(255),
    location VARCHAR(255),
    manager_id INT,
    FOREIGN KEY (manager_id)
        REFERENCES Manager (manager_id)
);

CREATE TABLE Contract (
    contract_id INT PRIMARY KEY,
    Estimated_cost Double,
    Completion_date date,
    manager_id INT,
    FOREIGN KEY (manager_id)
        REFERENCES Manager (manager_id)
);
CREATE TABLE Client (
    client_id INT PRIMARY KEY,
    name VARCHAR(255),
    location VARCHAR(255),
    contract_id INT,
    FOREIGN KEY (contract_id)
        REFERENCES Manager (contract_id)
);
